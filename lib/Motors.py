#!/usr/bin/python

#from Adafruit_MotorHAT import Adafruit_MotorHAT, Adafruit_DCMotor

import time
import atexit

class Motors:

    def __init__(self, is_test=False):
        self.is_test = is_test
        if not is_test:
            self.mh = Adafruit_MotorHAT()
            self.forward = Adafruit_MotorHAT.FORWARD
            self.reverse = Adafruit_MotorHAT.BACKWARD
        else:
            self.forward = 'FORWARD'
            self.reverse = 'REVERSE'
        
        self.max_speed = 255
        self.assignments = {
            'left': 1,
            'right': 2,
            'turret': 3
        }
        atexit.register(self.turnOffMotors)


    def turnOffMotors(self):
        for item in self.assignments:
            if not self.is_test:
                self.mh.getMotor(self.assignments[item]).run(Adafruit_MotorHAT.RELEASE) 
            else:
                print 'STOPPING: %s' % (self.assignments[item])   
        return True


    def control(self, l_percent, r_percent, turret_percent):

        l_direction = self.forward
        r_direction = self.forward
        turret_direction = self.forward

        if(float(l_percent) < 0):
            l_direction = self.reverse

        if(float(r_percent) < 0):
            r_direction = self.reverse

        if(float(turret_percent) < 0):
            turret_direction = self.reverse

        l_speed = (abs(int(float(l_percent) * 100)) * self.max_speed) / 100
        r_speed = (abs(int(float(r_percent) * 100)) * self.max_speed) / 100
        turret_speed = (abs(int(float(turret_percent) * 100)) * self.max_speed) / 100

        if not self.is_test:
            left_track = self.mh.getMotor(self.assignments['left'])
            right_track = self.mh.getMotor(self.assignments['right'])
            turret = self.mh.getMotor(self.assignments['turret'])

            left_track.run(l_direction)
            right_track.run(r_direction)
            turret.run(turret_direction)

            left_track.setSpeed(l_speed)
            right_track.setSpeed(r_speed)
            turret.setSpeed(turret_speed)
        else:
            print "L: %s (%s)  \tR: %s (%s)  \tT: %s (%s)" % (l_speed, l_direction, r_speed, r_direction, turret_speed, turret_direction)
        return True


    def turret_control(self, turret_percent):
        turret_direction = self.forward
        if(float(turret_percent) < 0):
            turret_direction = self.reverse

        turret_speed = (abs(int(float(turret_percent) * 100)) * self.max_speed) / 100

        if not self.is_test:
            turret = self.mh.getMotor(self.assignments['turret'])
            turret.run(turret_direction)
            turret.setSpeed(turret_speed)
        else:
            print "T: %s (%s)" % (turret_speed, turret_direction)

        return True




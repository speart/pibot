var pibot_logic = {

    control_obj: {
        "L": 0, // Left track motion
        "R": 0, // Right track motion
        "T": 0, // Turret rotation
        "P": 0, // Turret Pitch
        "F": false // Should Fire (Servo control)
    },

    desired_control_margin: 30,

    Circle: function () {
        this.x = 0;
        this.y = 0;
        this.radius = 0;
    },

    distance: function (circleA, circleB) {
        return Math.sqrt(((circleA.x - circleB.x) * (circleA.x - circleB.x)) + ((circleA.y - circleB.y) * (circleA.y - circleB.y)));
    },

    difference: function (circleA, circleB) {
        return Math.abs(circleA.radius - circleB.radius);
    },

    angle: function (circleOuter, circleInner) {
        var angle;
        if (circleInner.y === circleOuter.y) {
            if (circleInner.x >= circleOuter.x) {
                angle = 0;
            } else {
                angle = Math.PI;
            }
        } else if (circleInner.x === circleOuter.x) {
            if (circleInner.y >= circleOuter.y) {
                angle = Math.PI / 2;
            } else {
                angle = (3 * Math.PI) / 2;
            }
        } else {
            angle = Math.atan((circleInner.y - circleOuter.y) / (circleInner.x - circleOuter.x));
            if (circleInner.x > circleOuter.x && circleInner.y < circleOuter.y) {
                angle += 2 * Math.PI;
            } else if (circleInner.x < circleOuter.x) {
                angle += Math.PI;
            }
        }
        return angle;
    },

    magnitude: function (circleOuter, circleInner) {
        var mag = pibot_logic.distance(circleOuter, circleInner) / pibot_logic.difference(circleOuter, circleInner);
        if (mag > 1) {
            return 1;
        }
        return mag;
    },

    normalize : function (numberTuple) {
        var normalizedTuple = [];
        if (Math.abs(numberTuple[0]) > Math.abs(numberTuple[1])) {
            if (numberTuple[0] > 0) {
                normalizedTuple.push(1);
            } else {
                normalizedTuple.push(-1);
            }
            normalizedTuple.push(numberTuple[1] / (Math.abs(numberTuple[0])));
        } else {
            normalizedTuple.push(numberTuple[0] / (Math.abs(numberTuple[1])));
            if (numberTuple[1] > 0) {
                normalizedTuple.push(1);
            } else {
                normalizedTuple.push(-1);
            }
        }
        return normalizedTuple;
    },


    magnitudeTuple: function (magnitude, tuple) {
        var magTuple = [];
        magTuple.push(magnitude * tuple[0]);
        magTuple.push(magnitude * tuple[1]);
        return magTuple;
    },

    leftRightPowerTuple: function (outerCircle, innerCircle) {
        var mag = pibot_logic.magnitude(outerCircle, innerCircle);
        var tuple = [];
        tuple.push(Math.cos((pibot_logic.angle(outerCircle, innerCircle)) - (Math.PI / 4)));
        tuple.push(Math.sin((pibot_logic.angle(outerCircle, innerCircle)) - (Math.PI / 4)));
        var normalizedTuple = pibot_logic.normalize(tuple);
        return pibot_logic.magnitudeTuple(mag, normalizedTuple);
    },

    limit: function (x, y, details) {
        var dist = pibot_logic.distance({x: x, y: y}, {x: details.center[0], y: details.center[1]});
        if (dist <= details.radius) {
            return {x: x, y: y};
        }
        x = x - details.center[0];
        y = y - details.center[1];
        var radians = Math.atan2(y, x);
        return {
            x: Math.cos(radians) * details.radius + details.center[0],
            y: Math.sin(radians) * details.radius + details.center[1]
        };
    },

    sq_limit: function(x, y, details) {
        var x_in_bounds = (x > details.pos.left && x < details.pos.left + (details.radius * 2));
        var y_in_bounds = (y > details.pos.top && y < details.pos.top + (details.radius * 2));

        if(x_in_bounds && y_in_bounds) {
            return {x: x, y: y};
        } else if(x_in_bounds && !y_in_bounds) {

            return {
                x: x,
                y: (y > details.center[1]) ? (details.pos.top + (details.radius * 2)) : details.pos.top
            };
        } else if(!x_in_bounds && y_in_bounds) {
            return {
                x: (x > details.center[0]) ? (details.pos.left + (details.radius * 2)) : details.pos.left,
                y: y
            };
        } else {
            return {
                x: (x > details.center[0]) ? (details.pos.left + (details.radius * 2)) : details.pos.left,
                y: (y > details.center[1]) ? (details.pos.top + (details.radius * 2)) : details.pos.top
            };
        }
    },

    drive: function (el, details) {
        var offset = el.parent().offset();
        var result = pibot_logic.limit(details.coords[0], details.coords[1], details);
        
        var left = (result.x - (el.width() / 2)) - offset.left;
        var top = (result.y - (el.height() / 2)) - offset.top;

        var outerCircle = new pibot_logic.Circle();
        var innerCircle = new pibot_logic.Circle();
        outerCircle.radius = 100;
        innerCircle.radius = 25;
        innerCircle.x = result.y - (el.parent().height() / 2) - offset.top;
        innerCircle.y = result.x - (el.parent().width() / 2) - offset.left;
        var res = pibot_logic.leftRightPowerTuple(outerCircle, innerCircle);
        var left_dir = (res[0] < 0) ? '-' : '+';
        var right_dir = (res[1] < 0) ? '-' : '+';

        pibot_logic.state('L', Number(left_dir + Math.abs(Math.round(res[0] * 100) / 100)));
        pibot_logic.state('R', Number(right_dir + Math.abs(Math.round(res[1] * 100) / 100)));

        pibot_logic.control();

        el.css('left', left + "px");
        el.css('top', top + "px");
    },

    turret: function(el, details) {
        var offset = el.parent().offset();
        var result = pibot_logic.sq_limit(details.coords[0], details.coords[1], details);
        var left = (result.x - (el.width() / 2)) - offset.left;
        var top = (result.y - (el.height() / 2)) - offset.top;

        var min = -25;
        var max = 175;

        var top_pos = top + Math.abs(min);
        var left_pos = left + Math.abs(min);
        var middle_pos = (max + Math.abs(min)) / 2;
        var top_amount = (top_pos - middle_pos) / 100;
        var left_amount = (left_pos * 100) / (max + Math.abs(min)) / 100;

        pibot_logic.state('T', top_amount);
        pibot_logic.state('P', left_amount);

        pibot_logic.control();

        el.css('left', left + "px");
        el.css('top', top + "px");
    },

    all_stop: function (l_el, l_details, r_el, r_details) {
        pibot_logic.state('L', 0);
        pibot_logic.state('R', 0);
        pibot_logic.state('T', 0);
        pibot_logic.state('F', false);

        pibot_logic.control();

        var l_pos = l_el.parent().offset();
        var r_pos = r_el.parent().offset();

        l_el.animate({
            'left': ((l_details.center[0] - (l_el.width() / 2)) - l_pos.left) + 'px',
            'top': ((l_details.center[1] - (l_el.width() / 2)) - l_pos.top) + 'px'
        }, 200);

        r_el.animate({
            'top': ((r_details.center[1] - (r_el.width() / 2)) - r_pos.top) + 'px'
        }, 200);
    },

    state: function(key, val) {
        pibot_logic.control_obj[key] = val;
    },

    control: function () {
        window.socket.emit('control', pibot_logic.control_obj);
    },

    _console: function(message) {
        if($('.console').length) {
            $('.console').append('<div class="msg">' + message + '</div>');
            var objDiv = document.getElementsByClassName("console")[0];
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    },

    _render: function() {
        var $padRight = $('.pad.right');
        var $padLeft = $('.pad.left');

        var screenHeight = $(document).height();
        var padHeightRight = $padRight.outerHeight();
        var padHeightLeft = $padLeft.outerHeight();

        $padLeft.css({
            'marginTop': pibot_logic.desired_control_margin + 'px'
        });

        var rightMargin = screenHeight - (pibot_logic.desired_control_margin * 2) - (padHeightRight + padHeightLeft);
        $padRight.css({
            'marginTop': (screenHeight - (pibot_logic.desired_control_margin * 2) - (padHeightRight + padHeightLeft)) + 'px'
        });
    },

    _init: function (eventjs, io) {
        eventjs.add(document.body, 'contextmenu', eventjs.prevent);

        window.socket = io.connect(window.location.origin);
        window.socket.on('news', function (data) {
            var cons = document.getElementById('console');
            cons.innerHTML = data;
        });

        pibot_logic._render();

        var leftPadKnob = document.getElementsByClassName('left_knob')[0];
        var leftPad = document.getElementsByClassName('left')[0];
        eventjs.add(leftPad, "drag", function (event, self) {
            var pos = $(leftPadKnob).parent().offset();
            var details = {
                pos: pos,
                radius: $(leftPadKnob).parent().width() / 2,
                center: [pos.left + ($(leftPadKnob).parent().width() / 2), pos.top + ($(leftPadKnob).parent().height() / 2)],
                coords: [self.x + pos.left, self.y + pos.top]
            };
            pibot_logic._console('Left Knob: ' + details.center + ' = ' + details.coords)
            pibot_logic.drive($(leftPadKnob), details);
        });

        var rightPadKnob = document.getElementsByClassName('right_knob')[0];
        var rightPad = document.getElementsByClassName('right')[0];
        eventjs.add(rightPad, "drag", function (event, self) {
            var pos = $(rightPadKnob).parent().offset();
            var details = {
                pos: pos,
                radius: $(rightPadKnob).parent().width() / 2,
                center: [pos.left + ($(rightPadKnob).parent().width() / 2), pos.top + ($(rightPadKnob).parent().height() / 2)],
                coords: [self.x + pos.left, self.y + pos.top]
            };
            pibot_logic.turret($(rightPadKnob), details);
        });

        var fireBtn = document.getElementsByClassName('fire_btn')[0];
        eventjs.add(fireBtn, "mouseup", function () {
            pibot_logic.state('F', true);
            pibot_logic.control();
        });

        eventjs.add(document, "mouseup", function () {
            var l_pos = $(leftPadKnob).parent().offset();
            var l_details = {
                radius: $(leftPadKnob).parent().width() / 2,
                center: [l_pos.left + ($(leftPadKnob).parent().width() / 2), l_pos.top + ($(leftPadKnob).parent().height() / 2)]
            };

            var r_pos = $(rightPadKnob).parent().offset();
            var r_details = {
                radius: $(rightPadKnob).parent().width() / 2,
                center: [r_pos.left + ($(rightPadKnob).parent().width() / 2), r_pos.top + ($(rightPadKnob).parent().height() / 2)]
            };
            
            setTimeout(function () {
                pibot_logic.all_stop($(leftPadKnob), l_details, $(rightPadKnob), r_details);
            }, 10);

        });

    }
};
import socket
from lib.Motors import Motors

class TCPConnect:

	TCP_IP = '127.0.0.1'
	TCP_PORT = 6969
	BUFFER_SIZE = 1619200

	def connect(self):
		print("Starting PiBot Server...")
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.bind((self.TCP_IP, self.TCP_PORT))
		self.s.listen(1)
		self.motors = Motors(is_test=True)


	def exit(self):
		self.s.close()


	def handle_incoming(self):
		while 1:
			conn, addr = self.s.accept()
			#print 'Connection from:', addr
			data = conn.recv(self.BUFFER_SIZE)
			if not data: break
			
			incoming = data.split("\n")[7].split('&')

			controls = {}

			for item in incoming:
				part = item.split('=')
				controls[part[0]] = part[1].strip()

			self.motors.control(controls['L'], controls['R'], controls['T'])

			response_body_raw = '{"success": true}';

			response_headers = {
				'Content-Type': 'application/json; encoding=utf8',
				'Content-Length': len(response_body_raw),
				'Connection': 'close',
				'Access-Control-Allow-Origin': '*'
			}

			response_headers_raw = ''.join('%s: %s\n' % (k, v) for k, v in response_headers.iteritems())
			response = "HTTP/1.1 200 OK\n%s\n%s" % (response_headers_raw, response_body_raw)

			conn.send(response)
			conn.close()
		
		self.handle_incoming()


	def run(self):
		self.connect()
		self.handle_incoming()


if __name__ == "__main__":
	server = TCPConnect()
	try:
		server.run()
	except (KeyboardInterrupt, SystemExit):
		server.exit()
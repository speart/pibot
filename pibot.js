app = require('express.io')();
express = require('express');
var http = require('http');
var querystring = require('querystring');

app.http().io();

app.io.route('control', function(req) {
    console.log(req.data);

    var options = {
      host: 'localhost',
      port: '6969',
      path: '/',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    };

    var postData = querystring.stringify(req.data);

    var req = http.request(options, function(res) {
      res.setEncoding('utf8');
      /*
      res.on('data', function(chunk) {
        var chunk = JSON.parse(chunk);
      });
*/
    });

    req.on('error', function(e) {
      console.log('problem with request: ' + e.message);
    });

    req.write(postData);
    req.end();

});

app.get('/', function(req, res) {
    res.sendfile(__dirname + '/client.html');
});

app.use('/js', express.static('node_modules'));
app.use('/static', express.static('static'));


app.listen(5000);